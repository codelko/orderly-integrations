<?php
namespace Orderly\Integrations;

use Httpful\Request;
use Httpful\Mime;

class Connector{
    private $endpoint;
    private $domain;
    private $api_token;

    public function __construct($endpoint, $domain, $api_token){
        $this->endpoint = $endpoint;
        $this->domain = $domain;
        $this->api_token = $api_token;

        $response = Request::post($this->endpoint."/check")->addHeaders(["domain" => $this->domain, "api-token" => $this->api_token])->send();

        $body = $response->body;

        if($body->type != "success"){
            throw new \Exception($body->message);
        }
    }

    public function storeInvoice($data){
        $response = Request::post($this->endpoint."/execute")
        ->sendsType(Mime::FORM)
        ->withoutStrictSsl()
        ->expectsJson()  
        ->body('method=store-invoice&data='.$data)
        ->addHeaders(["domain" => $this->domain, "api-token" => $this->api_token])
        ->send();

        $body = $response->body;

        if($body->type != "success"){
            dd($body);
            throw new \Exception($body->message);
        }

        return $body;
    }

    public function cancelInvoice($data){
        $response = Request::post($this->endpoint."/execute")
        ->sendsType(Mime::FORM)
        ->withoutStrictSsl()
        ->expectsJson()  
        ->body('method=cancel-invoice&data='.$data)
        ->addHeaders(["domain" => $this->domain, "api-token" => $this->api_token])
        ->send();

        $body = $response->body;

        if($body->type != "success"){
            dd($body);
            throw new \Exception($body->message);
        }

        return $body;
    }
}